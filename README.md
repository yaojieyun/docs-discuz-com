# Discuz Q 文档项目

本项目使用 vuepress 进行文档开发，详细 vuepress使用说明可见 https://vuepress.vuejs.org/zh/

## 本地开发

clone 项目到本地后，使用 npm install 完成本地依赖的安装；

使用 `npm run dev` 命令启动本地 server，启动后可以在 http://localhost:8080/ 查看；

## 编译发布

使用 `npm run build` 命令进行编译，编译产物位于 docs/.vuepress/dist，可以将构建产物静态托管；


## 开发指引

### 文档目录

在 docs 目录下，基于大的类目进行文件夹创建，并在文件夹下使用 markdown 格式进行文档编写；

api-docs 目录用于 api 文档存储；

### 界面配置

通过在 docs/.vuepress/config.js 中， nav配置项可配置右上角相关链接；通过 sidebar 可以进行左侧栏设置，通过分组增加层次结构，并指定到特定目录下的文件；