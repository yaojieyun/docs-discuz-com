# 文本内容安全设置
### 概述

图片内容安全仅可“开启”和“关闭”，在云API未配置且没有开启之前无法点击。开启后用户发表的文本内容都会经过文本内容安全的鉴定，违规的内容会自动进入审核状态。本文将指导您如何设置并开启 Discuz! Q “文本内容安全” 设置。

### 前提条件
- 已[设置并开启云API](https://discuz.com/docs/%E4%BA%91%20API%20%E8%AE%BE%E7%BD%AE.html)。


### 操作指南

:::tip
- 设置文本内容安全请先配置云API，开通腾讯云文本内容安全服务，并确保有对应套餐包。
- 套餐包购买您可前往 [Discuz! Q 活动页](https://cloud.tencent.com/act/event/discuzq#welfare) 进行购买。
- 开通腾讯云文本内容安全服务详情操作参见：文本内容安全 [快速入门](https://cloud.tencent.com/document/product/1124/37119)。
:::

1. 使用 `http(s)://{站点名称}/admin` 登录 Discuz! Q 站点后台。
2. 在 Discuz! Q 站点后台中依次单击【全局】>【腾讯云设置】。
3. 单击 “文本内容安全” 处【开启】。如下图所示：

![](https://main.qcloudimg.com/raw/b3ec7d00efc48522fc45fb343f02873e.png)

4. 设置页面弹出“修改成功”后，即可完成文本内容安全设置操作。如下图所示：

![](https://main.qcloudimg.com/raw/3dcfcef146dcfa7d5fd563b026ae4309.png)