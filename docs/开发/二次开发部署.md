﻿# 二次开发部署
## 概述
本文将针对 Discuz! Q 二次开发的开发环境及代码部署进行说明。


## 环境配置
开发环境使用 Nginx 1.18.0 、PHP 7.3.23 、CentOS 7.6 版本为例。详情参见：[基于手动配置的 Nginx 环境](https://discuz.com/docs/Linux%20%E4%B8%BB%E6%9C%BA.html#nginx)。

## 代码部署
Discuz! Q 开源主仓库共有4个，若要 fork 仓库进行手动部署，则需要组合四个仓库的代码。详情如下：

```
Discuz-Q: git@gitee.com:Discuz/Discuz-Q.git  #Discuz! Q 后端主程序
Discuz-Q-Framework:  git@gitee.com:Discuz/Discuz-Q-Framework.git #Discuz! Q后端抽象类程序
Discuz-Q-uniapp: git@gitee.com:Discuz/Discuz-Q-uniapp.git  #Discuz! Q H5及小程序代码
Discuz-Q-Web: git@gitee.com:Discuz/Discuz-Q-Web.git   #Discuz! Q PC端代码
```

首次部署执行以下命令：

```
git clone git@gitee.com:Discuz/Discuz-Q.git dzq-dev
cd dzq-dev
git clone  git@gitee.com:Discuz/Discuz-Q-Framework.git 
framework
```

在 `dzq-dev` 目录中新增 `public_1` 与 `public_2`两个文件夹，并在设置如下值：
-  `public_1` 中新增 `skin.conf` 文件并写入 `1` 值到文件中。
- `public_2` 中新增 `skin.conf` 文件并写入 `2` 值到文件中。

:::tip
安装项目依赖前必须安装 composer ,您可以使用命令 `composer install` 进行安装。
:::

使用 `cd ../` 回到 `dzq-dev` 同级目录，使用 `clone` 命令拉取前端代码。

```
git clone  git@gitee.com:Discuz/Discuz-Q-uniapp discuz-uniapp
git clone  git@gitee.com:Discuz/Discuz-Q-Web.git discuz-web
```

:::tip
以下的脚本执行需要安装 npm 环境。
:::
把以下脚本内容复制到 back.sh 文件中。

```
#!/bin/bash
export PATH=$PATH:/usr/local/bin

BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $BASE/dzq-dev
git pull origin master

cd $BASE/dzq-dev/framework
git pull origin master

cd $BASE/dzq-dev/resources/frame

npm install
npm run build-admin
npm run build-admin:pay

rm -rf $BASE/dzq-dev/public_2/static-admin
cp -r $BASE/dzq-dev/public_pay/* $BASE/dzq-dev/public_2/ 

rm -rf $BASE/dzq-dev/public_1/static-admin
cp -r $BASE/dzq-dev/public/static-admin $BASE/dzq-dev/public_1/
cp  $BASE/dzq-dev/public/admin.html $BASE/dzq-dev/public_1/
```

把以下脚本内容复制到 H5.sh 文件中。
```
#!/bin/bash
export PATH=$PATH:/usr/local/bin

BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd $BASE/discuz-uniapp
git pull origin master

npm install
echo ============build h5====================
npm run build:h5
echo ============build h5 play===============
npm run build:h5-play

echo =============delete static====================
rm -rf $BASE/dzq-dev/public/static 
rm -rf $BASE/dzq-dev/public_1/static
rm -rf $BASE/dzq-dev/public_2/static

echo ============copy dist to public===============
cp -r $BASE/discuz-uniapp/dist/build/h5/* $BASE/dzq-dev/public/ 
cp -r $BASE/discuz-uniapp/dist/build/h5/* $BASE/dzq-dev/public_1/
cp -r $BASE/discuz-uniapp/dist/build/h5-play/* $BASE/dzq-dev/public_2/
```

把以下脚本内容复制到 pc.sh 文件中。

```
#!/bin/bash
export PATH=$PATH:/usr/local/bin

BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $BASE/discuz-web/
git pull origin master

npm install
npm run  build:spa
rm -rf $BASE/dzq-dev/public/invite 
rm -rf $BASE/dzq-dev/public/manage
rm -rf $BASE/dzq-dev/public/modify
rm -rf $BASE/dzq-dev/public/my
rm -rf $BASE/dzq-dev/public/_nuxt
rm -rf $BASE/dzq-dev/public/site
rm -rf $BASE/dzq-dev/public/thread
rm -rf $BASE/dzq-dev/public/topic
rm -rf $BASE/dzq-dev/public/user
mv dist/index.html dist/pc.html
cp -r $BASE/discuz-web/dist/* $BASE/dzq-dev/public/
rm -rf $BASE/dzq-dev/public_1/invite
rm -rf $BASE/dzq-dev/public_1/manage
rm -rf $BASE/dzq-dev/public_1/modify
rm -rf $BASE/dzq-dev/public_1/my
rm -rf $BASE/dzq-dev/public_1/_nuxt
rm -rf $BASE/dzq-dev/public_1/site
rm -rf $BASE/dzq-dev/public_1/thread
rm -rf $BASE/dzq-dev/public_1/topic
rm -rf $BASE/dzq-dev/public_1/user
cp -r $BASE/discuz-web/dist/* $BASE/dzq-dev/public_1/
rm -rf $BASE/discuz-web/dist
cd $BASE/discuz-web/
npm run  build:pay-spa
rm -rf $BASE/dzq-dev/public_2/invite
rm -rf $BASE/dzq-dev/public_2/manage
rm -rf $BASE/dzq-dev/public_2/modify
rm -rf $BASE/dzq-dev/public_2/my
rm -rf $BASE/dzq-dev/public_2/_nuxt
rm -rf $BASE/dzq-dev/public_2/site
rm -rf $BASE/dzq-dev/public_2/thread
rm -rf $BASE/dzq-dev/public_2/topic
rm -rf $BASE/dzq-dev/public_2/user
mv dist/index.html dist/pc.html
cp -r $BASE/discuz-web/dist/* $BASE/dzq-dev/public_2/
rm -rf $BASE/discuz-web/dist
```

把以上三个脚本文件`back.sh`、`H5.sh`、`pc.sh` 放置与 `dzq-dev` 文件同级目录下，并分别执行：

```
sh back.sh
sh H5.sh
sh pc.sh
```

构建并组合好所有的代码后，需使用 `git clone  git@gitee.com:Discuz/Discuz-Q-Update.git` 下载命令执行文件。

:::tip
若无权限，则需要从官方提供的安装包中，安装包 `/app/Console/Commands/Upgrades/` 目录下复制所有的文件到项目目录`/app/Console/Commands/Upgrades/`下。
:::

此处就已完成全部代码整合，配置好 Nginx 服务后访问 `https://域名/install` 即可安装站点。

:::danger
首次安装后，再次升级时，只需要执行 `back.sh`、`pc.sh`、`H5.sh`即可。数据迁移查看每个版本的命令，详情参见：
[手动升级方式](https://discuz.com/docs/%E5%B8%B8%E8%A7%84%E9%83%A8%E7%BD%B2%E5%8D%87%E7%BA%A7.html#%E6%89%8B%E5%8A%A8%E5%8D%87%E7%BA%A7%E6%96%B9%E5%BC%8F)。
:::




